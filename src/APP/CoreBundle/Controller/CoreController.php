<?php
namespace APP\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author Geoffrey
 */
class CoreController extends Controller {
    public function indexAction(){
        return $this->render('APPCoreBundle::faux-layout.html.twig');
    }
}

?>
