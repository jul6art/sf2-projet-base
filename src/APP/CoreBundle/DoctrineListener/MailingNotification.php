<?php
// src/OC/PlatformBundle/DoctrineListener/ApplicationNotification.php

namespace APP\CoreBundle\DoctrineListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use APP\UserBundle\Entity\User;
//importer chaque entite
//use APP\CoreBundle\Entity\importer chaque entite

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailingNotification
 *
 * @author Geoffrey
 */
class MailingNotification {
    


  private $mailer;

  public function __construct(\Swift_Mailer $mailer)
  {
    $this->mailer = $mailer;
  }

  public function postPersist(LifecycleEventArgs $args)
  {
    $entity = $args->getEntity();

    // On veut envoyer un email que pour les entités Application
    // //
    //a completer avec les entites
    if (($entity instanceof User)) {
        if($entity instanceof User){
            $sujet='Nouvel utilisateur inscrit';
            $contenu='Tu as un nouvel utilisateur signalé sur MonProjet.com.  Viens le voir: http://monprojet.com';
            $to='geoffreyk42@gmail.com';
        }
        $message = new \Swift_Message(
          $sujet,
          $contenu
        );

        $message
          ->addTo($to) // Ici bien sûr il faudrait un attribut "email", j'utilise "author" à la place
          ->addFrom('no_reply@monprojet.com')
        ;

        $this->mailer->send($message);
    }else{
        return;
    }
  }
}

?>
