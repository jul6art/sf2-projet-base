<?php

namespace APP\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class APPUserBundle extends Bundle
{
    public function getParent()
  {
    return 'FOSUserBundle';
  }
}
