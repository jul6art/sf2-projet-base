<?php

namespace APP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="APP\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    public function __construct()
  {
    parent::__construct();
// Par défaut, la date de l'annonce est la date d'aujourd'hui
//    $this->dateInscription = new \Datetime();
    $this->roles = ['ROLE_USER','ROLE_COMIQUE'];
//    $this->news=1;
//    $this->softDelete =0;
//    $this->nbMessages =0;
//    $this->nbFans =0;
//    $this->nbMembres =0;
//    $this->nbAbus =0;
  }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}